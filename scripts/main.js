var tabs = new Tabs();

/* Tabs */
function Tabs(tabs) {
	var tabs = this;

	/* change event */
	this.tabLinks = document.getElementsByClassName('tabs')[0];

	for(var i = 0, len = this.tabLinks.children.length; i < len; i++) {
		(function(index){
			tabs.tabLinks.children[i].onclick = function(ev){
				ev.preventDefault();
				tabs.hideTabs();
				tabs.changeTab(index);
			}
		})(i);
	}

	this.hideTabs = function() {
		var tabContents = document.getElementsByClassName('tab-content'), i;

		for (var i = 0; i < tabContents.length; i ++) {
			var element = tabContents[i];
			if ( (" " + element.className + " ").replace(/[\n\t]/g, " ").indexOf(" visible ") < 0 ) 
				element.style.display = 'none';
			element.className = element.className.replace(/\bvisible\b/,'');

			var clickedLink = tabs.tabLinks.children[i];

		}
	}

	this.changeTab = function(index) {
		/* deactivate tabs */
		for (var i = 0; i < this.tabLinks.children.length; i ++) {
			(function(index){
				tabs.tabLinks.children[i].className = tabs.tabLinks.children[i].className.replace(/\bactive\b/,'');
			})(i);
		}

		/* highlight tab */
		var clickedLink = this.tabLinks.children[index];
		clickedLink.className = clickedLink.className = clickedLink.className+' active';

		/* show content */
		var clickedTab = document.getElementsByClassName('tab-content')[index];
		clickedTab.style.display = 'block';

	}

	/* init hide tabs */
	this.hideTabs();

}